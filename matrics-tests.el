;;; matrics-tests.el --- tests for matrics.el -*- lexical-binding: t -*-

;; Copyright (C) 2020 Basil L. Contovounesios <contovob@tcd.ie>

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Code:

(require 'matrics)

(ert-deftest matrics-ub64 ()
  "Test Unpadded Base64 coding."
  (pcase-dolist (`(,in . ,out)
                 '(("" . "")
                   ("f" . "Zg")
                   ("fo" . "Zm8")
                   ("foo" . "Zm9v")
                   ("foob" . "Zm9vYg")
                   ("fooba" . "Zm9vYmE")
                   ("foobar" . "Zm9vYmFy")
                   ("\373\234\C-c\331~" . "+5wD2X4")
                   ("\C-t\373\234\C-c\331~" . "FPucA9l+")
                   ("\C-t\373\234\C-c\331?" . "FPucA9k/")))
    (let ((copy (copy-sequence in))
          (enc (matrics--ub64-encode in)))
      ;; Encoding doesn't modify in-place.
      (should (equal in copy))
      (should (equal enc out))
      ;; Round-trip.
      (should (equal (matrics--ub64-decode enc) in)))))

(ert-deftest matrics-split-uid ()
  "Test `matrics--split-uid'."
  (dolist (server '("matrix.org"
                    "matrix.org:8888"
                    "1.2.3.4"
                    "1.2.3.4:1234"
                    "[1234:5678::abcd]"
                    "[1234:5678::abcd]:5678"))
    (let ((uid   (concat "@x:" server))
          (split (cons "x" server)))
      (dolist (ext '(nil t))
        (should (equal (matrics--split-uid uid ext) split))))
    (let ((ext (concat "@!:" server)))
      (should-not (matrics--split-uid ext))
      (should (equal (matrics--split-uid ext t) (cons "!" server))))
    (let ((ext (concat "@ :" server)))
      (should-not (matrics--split-uid ext))
      (should-not (matrics--split-uid ext t)))))

(provide 'matrics-tests)
