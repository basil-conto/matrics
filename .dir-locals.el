((nil
  (fill-column . 70)
  (sentence-end-double-space . t)
  (tab-width . 8))
 (c-mode
  (c-file-style . "GNU")
  (indent-tabs-mode . nil))
 (emacs-lisp-mode
  (indent-tabs-mode . nil)))
