;;; matrics.el --- matrix chat client -*- lexical-binding: t -*-

;; Copyright (C) 2020 Basil L. Contovounesios <contovob@tcd.ie>

;; Author: Basil L. Contovounesios <contovob@tcd.ie>
;; Created: 2020-10-24
;; Keywords: comm
;; URL: https://gitlab.com/basil-conto/matrics
;; Version: 0.0.1

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Matrix chat client for Emacs.

;; For more information on Matrix, see https://matrix.org/.

;; TODO:
;; - describe capabilities
;; - describe how to begin using the client once installed

;;; Code:

(eval-when-compile
  (require 'subr-x))

(autoload 'auth-source-search "auth-source")

(defgroup matrics ()
  "Matrix chat client."
  :package-version '(matrics . "0.0.1")
  :group 'applications
  :group 'comm)

(defvar matrics--default-server "matrix.org"
  "URL of default Matrix homeserver.")

(defvar matrics--default-user user-login-name
  "Default Matrix username.")

;;;; User options and faces

(defcustom matrics-server 'prompt
  "URL of the user's Matrix homeserver.

If `prompt' (the default), the URL is prompted interactively.
  If the URL from the last `matrics' session is found in
  `matrics-save-file', it is included in the prompt as the
  default suggestion.
If `saved', use the URL saved in `matrics-save-file'.  If none is
  found, fall back to prompting the user as with `prompt'.
If a string, use that homeserver URL without consulting
  `matrics-save-file' or prompting.

See also the user option `matrics-save'."
  :package-version '(matrics . "0.0.1")
  :group 'matrics
  :type `(choice (string :tag "Homeserver URL" :value ,matrics--default-server)
                 (const :tag "Prompt for URL" prompt)
                 (const :tag "Use saved URL" saved)))

(defcustom matrics-user 'prompt
  "The user's username on `matrics-server'.

If `prompt' (the default), the username is prompted
  interactively.  If the username from the last `matrics' session
  is found in `matrics-save-file', it is included in the prompt
  as the default suggestion.
If `saved', use the username saved in `matrics-save-file'.
  If none is found, fall back to prompting the user as with
  `prompt'.
If a string, use that username without consulting
  `matrics-save-file' or prompting.

See also the user option `matrics-save'."
  :package-version '(matrics . "0.0.1")
  :group 'matrics
  :type `(choice (string :tag "Username" :value ,matrics--default-user)
                 (const :tag "Prompt for username" prompt)
                 (const :tag "Use saved username" saved)))

(defcustom matrics-save nil
  "Whether to persist Matrix homeserver and user information.

If non-nil, this information is persisted across `matrics'
sessions by writing to `matrics-save-file'.  You can then set
`matrics-server' and `matrics-user' to `saved' to automatically
reuse the last session's details.

If nil (the default), no homeserver or user information is
written to disk.  Note that a nil value does not cause an
existing `matrics-save-file' to be deleted; for that, call the
command `matrics-delete-save-file'."
  :package-version '(matrics . "0.0.1")
  :group 'matrics
  :type 'boolean)

(defcustom matrics-save-file (locate-user-emacs-file "matrics/save.el")
  "File persisting Matrix homeserver and user information.
This file is only used if `matrics-save' (which see) is non-nil."
  :package-version '(matrics . "0.0.1")
  :group 'matrics
  :type 'file)

(defface matrics-pending '((t :inherit shadow))
  "Face for pending messages in Matrix rooms."
  :package-version '(matrics . "0.0.1")
  :group 'matrics)

;;;; Internal definitions

(defvar matrics--server nil "Current Matrix homeserver URL.")
(defvar matrics--domain nil "Domain namespace of `matrics--server'.")
(defvar matrics--token nil "Authentication token for `matrics--server'.")
(defvar matrics--saved () "Alist contents of `matrics-save-file'.")
(defvar matrics--host nil "Host component of `matrics--server'.")
(defvar matrics--user nil "Current user on `matrics--server'.")
(defvar matrics--txnid 0 "Monotonically increasing transaction identifier.")

(define-error 'matrics-error "Matrics error")

(defvar url-request-data)
(defvar url-request-extra-headers)
(defvar url-request-method)

;;;; Utilities

(defun matrics--ub64-encode (str)
  "Return STR encoded as Unpadded Base64."
  (setq str (base64url-encode-string str t))
  (subst-char-in-string ?- ?+ (subst-char-in-string ?_ ?/ str t) t))

(defun matrics--ub64-decode (str)
  "Return STR decoded as Unpadded Base64."
  (setq str (subst-char-in-string ?+ ?- (subst-char-in-string ?/ ?_ str) t))
  (base64-decode-string str t))

;; Server name.
(rx-define matrics--server
  (: (| (: (** 1 3 num) (= 3 ?. (** 1 3 num))) ; IPv4.
        (: ?\[ (** 2 45 (in hex ?: ?.)) ?\])   ; IPv6.
        (** 1 255 (in alnum ?- ?.)))           ; DNS.
     (? ?: (** 1 5 num))))                     ; Port.

;; User ID format.
(rx-define matrics--uid-fmt (char)
  (: ?@ (group (+? char)) ?: (group matrics--server)))

;; New, stricter user IDs.
(rx-define matrics--uid (matrics--uid-fmt (in num (?a . ?z) ?- ?. ?= ?_ ?/)))

;; Old, extended user IDs.
(rx-define matrics--uid-ext (matrics--uid-fmt (in (?! . ?9) (?\; . ?~))))

(defun matrics--split-uid (uid &optional extended)
  "Split Matrix UID into (LOCALPART . DOMAIN).
With non-nil EXTENDED, recognise the older, extended user ID
character set.  Return nil if UID is not a valid user ID."
  (and (string-match (if extended (rx matrics--uid-ext) (rx matrics--uid)) uid)
       (cons (match-string 1 uid) (match-string 2 uid))))

(defun matrics--url-check (status)
  "Maybe display appropriate error for `url-retrieve' STATUS."
  (defvar url-current-object)
  (defvar url-http-codes)
  (declare-function url-host "url-parse" (cl-x))
  (pcase (plist-get status :error)
    ((pred null))
    (`(error http ,(and (pred numberp) code))
     (setq code (assq code url-http-codes))
     (lwarn 'matrics :error "%s: HTTP %s: %s"
            (url-host url-current-object)
            (car code) (caddr code)))
    (err (lwarn 'matrics :error "%s" err))))

(defun matrics--url-display (status)
  "Display current buffer returned by `url-retrieve'."
  (matrics--url-check status)
  (display-buffer (current-buffer)))

(defun matrics-versions ()
  (interactive)
  (url-retrieve (concat matrics-server "_matrix/client/versions")
                #'matrics--url-display))

(defun matrics--room-id (alias)
  (url-retrieve (concat matrics-server "_matrix/client/r0/directory/room/"
                        (url-hexify-string (concat alias ":" matrics--domain)))
                #'matrics--url-display))

(defun matrics--send-message (roomid)
  (let ((url-request-method "POST")
        (url-request-extra-headers
         `(("Content-Type" . "application/json")
           ("Authorization" . ,(concat "Bearer " matrics--token))))
        (url-request-data (json-serialize '((msgtype . "m.text")
                                            (body . "hello, world!")))))
    (url-retrieve (concat matrics-server
                          "_matrix/client/r0/rooms/"
                          (url-hexify-string roomid)
                          "/send/m.room.message")
                  #'matrics--url-display)))

(defun matrics--login-password ()
  (let* ((pwd (auth-source-search :max 1 :host matrics--host
                                  :require '(:secret)))
         (pwd (if pwd
                  (funcall (plist-get (car pwd) :secret))
                (read-passwd (format "Password for %s: " matrics--host))))
         (url-request-method "POST")
         (url-request-extra-headers '(("Content-Type" . "application/json")))
         (url-request-data (json-serialize
                            `((type . "m.login.password")
                              (identifier (type . "m.id.user")
                                          (user . ,matrics-user))
                              (password . ,pwd)))))
    (url-retrieve (concat matrics-server "_matrix/client/r0/login")
                  #'matrics--url-display)))

(defun matrics--login-flows (status)
  (matrics--url-check status)
  (search-forward "\n\n")
  (let ((alist (json-parse-buffer :object-type 'alist)))
    (when (seq-some (lambda (flow)
                      (equal (alist-get 'type flow) "m.login.password"))
                    (alist-get 'flows alist))
      (matrics--login-password))))

(defun matrics-login ()
  (interactive)
  (url-retrieve (concat matrics-server "_matrix/client/r0/login")
                #'matrics--login-flows))

;;;; UI

(define-derived-mode matrics-home-mode special-mode "mtx-home"
  "Major mode for Matrix homeservers."
  :group 'matrics
  (setq buffer-read-only t)
  (buffer-disable-undo))

(define-derived-mode matrics-room-mode special-mode "mtx-room"
  "Major mode for Matrix rooms."
  :group 'matrics
  (setq buffer-read-only t)
  (buffer-disable-undo))

(define-derived-mode matrics-text-mode text-mode "mtx-text"
  "Major mode for Matrix text buffers."
  :group 'matrics)

(defun matrics--read-save-file ()
  "Read `matrics-save-file' into `matrics--saved' and return it."
  (when (file-exists-p matrics-save-file)
    (with-temp-buffer
      (insert-file-contents matrics-save-file)
      (setq matrics--saved (read (current-buffer))))))

(defun matrics--write-save-file ()
  "Write `matrics--saved' into `matrics-save-file'."
  (with-file-modes #o600
    (with-temp-file matrics-save-file
      (insert ";;; " (file-name-nondirectory matrics-save-file)
              " --- save file for matrics.el -*- lisp-data -*-\n\n")
      (let (print-length print-level)
        (pp matrics--saved (current-buffer))))))

(defun matrics-delete-save-file ()
  "Delete `matrics-save-file'."
  (interactive)
  (when (yes-or-no-p (format "Delete file %s? " matrics-save-file))
    (delete-file matrics-save-file)))

(defun matrics--normalize-server (url)
  "Return normalized Matrix homeserver URL.
Ensure its scheme is HTTPS if it lacks one; otherwise barf if
it's not HTTP[S].  Ensure URL ends in a slash."
  (setq url (string-trim url))
  (let* ((urlobj (url-generic-parse-url url))
         (scheme (url-type urlobj)))
    (cond ((member scheme '("https" "http")))
          (scheme (user-error "Unsupported URI scheme: %s" scheme))
          ((setq url (concat "https://" url)))))
  (if (string-suffix-p "/" url) url (concat url "/")))

(defvar matrics--server-history ()
  "History for reading Matrix homeserver URLs.")

(defvar matrics--user-history ()
  "History for reading Matrix usernames.")

(defun matrics--set-server ()
  "Initialize server-related variables."
  (let (server host user)
    (cond ((stringp matrics-server)
           (setq server matrics-server))
          ((and (setq server (alist-get 'server (matrics--read-save-file)))
                (setq host (url-host (url-generic-parse-url server)))
                (eq matrics-server 'saved)))
          ((let ((def (delq nil (list host matrics--default-server))))
             (setq server (read-string (format-prompt "Homeserver URL" def)
                                       nil 'matrics--server-history def)))))
    (setq server (matrics--normalize-server server))
    (setq host (url-host (url-generic-parse-url server)))
    (cond ((stringp matrics-user)
           (setq user matrics-user))
          ((and (setq user (alist-get 'user matrics--saved))
                (eq matrics-user 'saved)))
          ((let ((def (delq nil (list user matrics--default-user))))
             (setq user (read-string (format-prompt "Username on %s" def host)
                                     nil 'matrics--user-history def)))))
    (setq matrics--server server)
    (setq matrics--host host)
    (setq matrics--user user)
    (when matrics-save
      (setf (alist-get 'server matrics--saved) server)
      (setf (alist-get 'user matrics--saved) user)
      (matrics--write-save-file))))

(defun matrics--discover-server ()
  "")

(defun matrics ()
  "Start Matrix chat."
  (interactive)
  (unless matrics--server
    (matrics--discover-server)))

(provide 'matrics)

;;; matrics.el ends here
