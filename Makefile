# Makefile for matrics.el

# Copyright (C) 2020 Basil L. Contovounesios <contovob@tcd.ie>

# This file is NOT part of GNU Emacs.

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

EMACS ?= emacs
PKG := matrics

%.elc: %.el
	$(EMACS) --quick --batch --funcall=batch-byte-compile $<

all: $(PKG)

$(PKG): $(PKG).elc

.PHONY: emacs-Q
emacs-Q:
	$(EMACS) --quick --load=$(PKG)$(if $(filter 1 yes true,$(ELC)),.elc,.el)

test: $(PKG)
	$(EMACS) --quick --batch --directory=$(CURDIR) --load=$(PKG)-tests.el \
		--funcall=ert-run-tests-batch-and-exit

.PHONY: clean
clean:
	$(RM) $(PKG).elc
